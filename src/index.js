import $ from "jquery";
import _ from "lodash";
import * as THREE from "three";
import "./css/bundle.less";

let renderer;
let cameraOrtho, sceneOrtho;
let sprites;
let keys = [];
let views = [];
const KEYUP = 38;
const KEYDOWN = 40;
const KEYLEFT = 37;
const KEYRIGHT = 39;
const KEY_PGUP = 33;
const KEY_PGDOWN = 34;
const KEY_END = 35;
const KEY_HOME = 36;
const KEY_Q = 81;
const KEY_A = 65;
const KEY_J = 74;
const KEY_K = 75;
const KEY_SPACE = 32;
const HORIZONTAL = 0;
const VERTICAL = 1;

let numViews = 2;
let width = window.innerWidth;
let height = window.innerHeight;
let splitWidth = width / numViews;

let state, tstate;
function reset(onlyTarget) {
  if (!onlyTarget) {
    state = {
      zoom: 1,
      offset: { x: 0, y: 0 },
      depthoffset: 0,
    };
  }
  tstate = {
    zoom: 1,
    offset: { x: 0, y: 0 },
    depthoffset: 0,
  };
}
reset();
var dirty = true;

var keyMappings = {
  [KEYUP]: () => (tstate.offset.y -= Math.max(Math.pow(2, state.zoom - 1), 1)),
  [KEYDOWN]: () =>
    (tstate.offset.y += Math.max(Math.pow(2, state.zoom - 1), 1)),
  [KEYLEFT]: () =>
    (tstate.offset.x += Math.max(Math.pow(2, state.zoom - 1), 1)),
  [KEYRIGHT]: () =>
    (tstate.offset.x -= Math.max(Math.pow(2, state.zoom - 1), 1)),
  [KEY_A]: () => (tstate.zoom -= 0.01),
  [KEY_Q]: () => (tstate.zoom += 0.01),
  [KEY_J]: () => (tstate.depthoffset -= 0.25),
  [KEY_K]: () => (tstate.depthoffset += 0.25),
  [KEY_SPACE]: () => reset(true),
};

init();
animate();

function init() {
  var orientation = HORIZONTAL;

  views = Array.from(Array(numViews).keys()).map((i) => {
    const sprite = new THREE.Sprite();
    sprite.scale.set(300, 300, 1);
    sprite.position.set(0, 0, 1);

    const camera = new THREE.OrthographicCamera(
      -width / 2 / numViews,
      width / 2 / numViews,
      height / 2,
      -height / 2,
      1,
      10
    );
    camera.position.z = 10;
    const scene = new THREE.Scene();

    scene.add(sprite);
    recalculatePosition(sprite, i);

    return {
      sprite,
      camera,
      scene,
    };
  });

  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(width, height);
  renderer.autoClear = false; // To allow render overlay on top of sprited sphere

  document.body.appendChild(renderer.domElement);

  window.addEventListener("resize", onWindowResize, false);
  document.addEventListener("keydown", (e) => (keys[e.keyCode] = true));
  document.addEventListener("keyup", (e) => (keys[e.keyCode] = false));
  document.addEventListener("keydown", (e) => {
    if (e.keyCode === KEY_PGUP || e.keyCode === KEY_HOME) {
      currentFileIndex++;
      if (currentFileIndex >= files.length) {
        currentFileIndex = 0;
      }
      loadImageIndex(currentFileIndex);
    } else if (e.keyCode === KEY_PGDOWN || e.keyCode === KEY_END) {
      currentFileIndex--;
      if (currentFileIndex < 0) {
        currentFileIndex = files.length - 1;
      }
      loadImageIndex(currentFileIndex);
    }
  });
}

function animate() {
  requestAnimationFrame(animate);

  Object.keys(keyMappings).forEach((key) => {
    if (keys[key]) {
      keyMappings[key]();
    }
  });

  dirty = dirty || !_.isEqual(state, tstate);

  lerpObject(state, tstate);

  if (dirty) {
    updateAll();
    render();
    dirty = false;
  }
}

function render() {
  renderer.clear();

  views.forEach((v, i) => {
    renderer.setViewport(i * splitWidth, 0, splitWidth, height);
    renderer.render(v.scene, v.camera);
  });
}

function onWindowResize() {
  width = window.innerWidth;
  height = window.innerHeight;
  splitWidth = width / numViews;

  views.forEach((v) => {
    var camera = v.camera;
    camera.left = -width / 2 / numViews;
    camera.right = width / 2 / numViews;
    camera.top = height / 2;
    camera.bottom = -height / 2;
    camera.updateProjectionMatrix();
    recalculateSizeAndPosition(v.sprite);
  });

  renderer.setSize(width, height);
  dirty = true;
}

function updateImage(img) {
  views.forEach((v, i) => {
    var texture = new THREE.Texture(
      img,
      undefined,
      undefined,
      undefined,
      undefined,
      THREE.LinearFilter
    );
    texture.needsUpdate = true;
    var material = (v.sprite.material = new THREE.SpriteMaterial({
      map: texture,
    }));

    // 0 = left half, 0.5 = right half
    material.map.offset.x = i === 0 ? 0 : 0.5;
    material.map.offset.y = 0;
    // 0.5 = cut in half
    material.map.repeat.x = 0.5;
    material.map.repeat.y = 1;

    recalculateSizeAndPosition(v.sprite, i);
  });
  dirty = true;
}

function updateAll() {
  views.forEach((v, i) => recalculateSizeAndPosition(v.sprite, i));
}

function recalculateSizeAndPosition(sprite, index) {
  var material = sprite.material;
  if (!material || !material.map || !material.map.image) {
    return;
  }
  var img = material.map.image;

  recalculatePosition(sprite, index);

  var scale = {
    x: 0.5,
    y: 1,
  };

  var logZoom = Math.pow(2, state.zoom - 1);

  // width divided by 2 because it has been cut in half
  var imgAspect = ((img.width * scale.x) / 2 / img.height) * scale.y;
  // compare aspects
  var newSize = {};
  if (width / height > imgAspect) {
    newSize.x = height * imgAspect * logZoom;
    newSize.y = height * logZoom;
  } else {
    newSize.x = width * logZoom;
    newSize.y = (width / imgAspect) * logZoom;
  }

  sprite.scale.set(newSize.x, newSize.y);
}

function recalculatePosition(sprite, index) {
  sprite.position.set(
    state.offset.x + (index === 0 ? state.depthoffset : -state.depthoffset),
    state.offset.y,
    1
  );
}

function lerp(source, target) {
  var diff = target - source;
  if (Math.abs(diff / target) < 0.0001) {
    return target;
  }
  return (source += diff * 0.1);
}
function lerpObject(source, target, keys) {
  if (typeof keys === "undefined") {
    keys = Object.keys(source);
  }
  keys.forEach((k) => {
    if (_.isObject(source[k])) {
      lerpObject(source[k], target[k]);
    } else {
      source[k] = lerp(source[k], target[k]);
    }
  });
}

function imageLoaded(src) {
  var img = new Image();
  img.onload = () => {
    updateImage(img);
  };
  img.onerror = () => {
    alert("Could not read image");
  };
  img.src = src;
}

let files = [];
let currentFileIndex = null;

function loadImageIndex(i) {
  if (!files[i]) {
    return;
  }
  var reader = new FileReader();
  reader.onload = (e) => {
    imageLoaded(reader.result);
  };
  reader.readAsDataURL(files[i]);
  currentFileIndex = i;
}

$("#loadfile input[type=file]").on("change", (e) => {
  files = e.target.files;
  loadImageIndex(0);
  e.target.blur();
});
